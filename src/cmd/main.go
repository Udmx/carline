package main

import (
	"github.com/udmx/carline/api"
	"github.com/udmx/carline/config"
	"github.com/udmx/carline/data/cache"
	"github.com/udmx/carline/data/db"
	"github.com/udmx/carline/data/db/migrations"
	"github.com/udmx/carline/pkg/logging"
)

// @securityDefinitions.apikey AuthBearer
// @in header
// @name Authorization
func main() {

	cfg := config.GetConfig()

	logger := logging.NewLogger(cfg)

	err := cache.InitRedis(cfg)
	defer cache.CloseRedis()
	if err != nil {
		logger.Fatal(logging.Redis, logging.Startup, err.Error(), nil)
	}

	err = db.InitDb(cfg)
	defer db.CloseDb()
	if err != nil {
		logger.Fatal(logging.Postgres, logging.Startup, err.Error(), nil)
	}
	migrations.Up_1()

	api.InitServer(cfg)
}
