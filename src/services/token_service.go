package services

import (
	"github.com/golang-jwt/jwt"
	"github.com/udmx/carline/api/dto"
	"github.com/udmx/carline/config"
	"github.com/udmx/carline/constants"
	"github.com/udmx/carline/pkg/logging"
	"github.com/udmx/carline/pkg/service_errors"
	"time"
)

type TokenService struct {
	logger logging.Logger
	cfg    *config.Config
}

type tokenDto struct {
	UserId       int
	FirstName    string
	LastName     string
	UserName     string
	MobileNumber string
	Email        string
	Roles        []string
}

func NewTokenService(cfg *config.Config) *TokenService {
	return &TokenService{
		logger: logging.NewLogger(cfg),
		cfg:    cfg,
	}
}

func (s *TokenService) GenerateToken(token *tokenDto) (*dto.TokenDetails, error) {
	tokenDetails := &dto.TokenDetails{}
	tokenDetails.AccessTokenExpireTime = time.Now().Add(s.cfg.JWT.AccessTokenExpireDuration * time.Minute).Unix()
	tokenDetails.RefreshTokenExpireTime = time.Now().Add(s.cfg.JWT.RefreshTokenExpireDuration * time.Minute).Unix()

	accessTokenClaims := jwt.MapClaims{}

	accessTokenClaims[constants.UserIdKey] = token.UserId
	accessTokenClaims[constants.FirstNameKey] = token.FirstName
	accessTokenClaims[constants.LastNameKey] = token.LastName
	accessTokenClaims[constants.UsernameKey] = token.UserName
	accessTokenClaims[constants.EmailKey] = token.Email
	accessTokenClaims[constants.MobileNumberKey] = token.MobileNumber
	accessTokenClaims[constants.RolesKey] = token.Roles
	accessTokenClaims[constants.ExpireTimeKey] = tokenDetails.AccessTokenExpireTime

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, accessTokenClaims)

	var err error
	tokenDetails.AccessToken, err = accessToken.SignedString([]byte(s.cfg.JWT.Secret))
	if err != nil {
		return nil, err
	}

	refreshTokenClaims := jwt.MapClaims{}

	refreshTokenClaims[constants.UserIdKey] = token.UserId
	refreshTokenClaims[constants.ExpireTimeKey] = tokenDetails.RefreshTokenExpireTime

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, refreshTokenClaims)

	tokenDetails.RefreshToken, err = refreshToken.SignedString([]byte(s.cfg.JWT.RefreshSecret))
	if err != nil {
		return nil, err
	}

	return tokenDetails, nil
}

func (s *TokenService) VerifyToken(token string) (*jwt.Token, error) {
	t, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, &service_errors.ServiceError{EndUserMessage: service_errors.UnExpectedError}
		}
		return []byte(s.cfg.JWT.Secret), nil
	})
	if err != nil {
		return nil, err
	}
	return t, nil
}

func (s *TokenService) GetClaims(token string) (ClaimMap map[string]interface{}, err error) {
	ClaimMap = map[string]interface{}{}
	verifyToken, err := s.VerifyToken(token)
	if err != nil {
		return nil, err
	}
	claims, ok := verifyToken.Claims.(jwt.MapClaims)
	if ok && verifyToken.Valid {
		for k, v := range claims {
			ClaimMap[k] = v
		}
		return ClaimMap, nil
	}
	return nil, &service_errors.ServiceError{EndUserMessage: service_errors.ClaimsNotFound}
}
