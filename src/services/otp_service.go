package services

import (
	"fmt"
	"time"

	"github.com/go-redis/redis/v7"
	"github.com/udmx/carline/config"
	"github.com/udmx/carline/constants"
	"github.com/udmx/carline/data/cache"
	"github.com/udmx/carline/pkg/logging"
	"github.com/udmx/carline/pkg/service_errors"
)

type OtpService struct {
	logger      logging.Logger
	cfg         *config.Config
	redisClient *redis.Client
}

type OtpDto struct {
	Value string
	Used  bool
}

func NewOtpService(cfg *config.Config) *OtpService {
	return &OtpService{
		logger:      logging.NewLogger(cfg),
		cfg:         cfg,
		redisClient: cache.GetRedis(),
	}
}

func (s *OtpService) SetOtp(mobileNumber string, otp string) error {
	key := fmt.Sprintf("%s:%s", constants.RedisOtpDefaultKey, mobileNumber)
	val := &OtpDto{
		Value: otp,
		Used:  false,
	}

	res, err := cache.Get[OtpDto](s.redisClient, key) //if there isn't key in redis, It has an error
	if err == nil && !res.Used {
		return &service_errors.ServiceError{
			EndUserMessage: service_errors.OtpExists,
		}
	} else if err == nil && res.Used {
		return &service_errors.ServiceError{
			EndUserMessage: service_errors.OtpUsed,
		}
	}
	err = cache.Set(s.redisClient, key, val, s.cfg.Otp.ExpireTime*time.Second)
	if err != nil {
		return err
	}
	return nil
}

func (s *OtpService) ValidateOtp(mobileNumber string, otp string) error {
	key := fmt.Sprintf("%s:%s", constants.RedisOtpDefaultKey, mobileNumber)
	res, err := cache.Get[OtpDto](s.redisClient, key)
	if err != nil {
		return err
	} else if err == nil && res.Used {
		return &service_errors.ServiceError{
			EndUserMessage: service_errors.OtpUsed,
		}
	} else if err == nil && !res.Used && res.Value != otp {
		return &service_errors.ServiceError{
			EndUserMessage: service_errors.OtpInvalid,
		}
	} else if err == nil && !res.Used && res.Value == otp {
		res.Used = true
		err = cache.Set(s.redisClient, key, res, s.cfg.Otp.ExpireTime*time.Second)
		if err != nil {
			return err
		}
		return nil
	}
	return nil
}
