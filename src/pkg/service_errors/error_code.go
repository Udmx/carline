package service_errors

const (
	// Token
	UnExpectedError = "Expected error"
	ClaimsNotFound  = "Claims not found"
	TokenRequired   = "Token required"
	TokenExpired    = "Token expired"
	TokenInvalid    = "Token invalid"

	// Otp
	OtpExists  = "Otp exists"
	OtpUsed    = "Otp used"
	OtpInvalid = "Otp invalid"

	// User
	EmailExists      = "Email exists"
	UsernameExists   = "Username exists"
	PermissionDenied = "Permission denied"

	// DB
	RecordNotFound = "Record not found"
)
