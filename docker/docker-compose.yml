version: '3.7'

services:
  ####################### SETUP #######################
  setup:
    build:
      context: elk/setup/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    init: true
    volumes:
      - ./elk/setup/entrypoint.sh:/entrypoint.sh:ro,Z
      - ./elk/setup/helpers.sh:/helpers.sh:ro,Z
      - ./elk/setup/roles:/roles:ro,Z
      - setup:/state:Z
    environment:
      ELASTIC_PASSWORD: ${ELASTIC_PASSWORD:-}
      KIBANA_SYSTEM_PASSWORD: ${KIBANA_SYSTEM_PASSWORD:-}
      FILEBEAT_INTERNAL_PASSWORD: ${FILEBEAT_INTERNAL_PASSWORD:-}
    networks:
      - webapi_network
    depends_on:
      - elasticsearch
  ####################### ELK #######################
  elasticsearch:
    build:
      context: elk/elasticsearch/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    container_name: elasticsearch
    volumes:
      - ./elk/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml:ro,Z
      - elasticsearch:/usr/share/elasticsearch/data:Z
    ports:
      - 9200:9200
      - 9300:9300
    environment:
      node.name: elasticsearch
      ES_JAVA_OPTS: -Xms512m -Xmx512m
      ELASTIC_PASSWORD: ${ELASTIC_PASSWORD:-}
      discovery.type: single-node
    networks:
      - webapi_network
    restart: unless-stopped

  kibana:
    build:
      context: elk/kibana/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    volumes:
      - ./elk/kibana/config/kibana.yml:/usr/share/kibana/config/kibana.yml:ro,Z
      - elasticsearch:/usr/share/kibana/data:Z
    ports:
      - 5601:5601
    environment:
      KIBANA_SYSTEM_PASSWORD: ${KIBANA_SYSTEM_PASSWORD:-}
    networks:
      - webapi_network
    depends_on:
      - elasticsearch
    restart: unless-stopped

  filebeat:
    build:
      context: elk/filebeat/
      args:
        ELASTIC_VERSION: ${ELASTIC_VERSION}
    user: root
    command:
      - -e
      - --strict.perms=false
    volumes:
      - ./elk/filebeat/config/filebeat.yml:/usr/share/filebeat/filebeat.yml
      - ../src/logs:/var/log/filebeat/service
      - ../prod/logs:/var/log/filebeat/service
      - logs:/var/log/filebeat
      - logs:/app:ro

    environment:
      FILEBEAT_INTERNAL_PASSWORD: ${FILEBEAT_INTERNAL_PASSWORD:-}
      BEATS_SYSTEM_PASSWORD: ${FILEBEAT_INTERNAL_PASSWORD:-}
    networks:
      - webapi_network
    depends_on:
      - elasticsearch

  ####################### POSTGRES #######################
  postgres:
    image: postgres
    container_name: postgres_container
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: admin
      POSTGRES_DB: carline_db #create default DB
      PGDATA: /data/postgres
    volumes: #define volume for postgres
      - postgres:/data/postgres #please send data to my volume(postgres) which define before
    ports:
      - "5432:5432" # export port for postgres (example:"host:container_port")
    networks:
      - webapi_network #what is my network
    restart: unless-stopped #if your container failed, It would not restart, your container would stop to see log by you

  pgadmin:
    image: dpage/pgadmin4
    container_name: pgadmin_container
    environment:
      PGADMIN_DEFAULT_EMAIL: pouriashafieeit@gmail.com
      PGADMIN_DEFAULT_PASSWORD: 123456
    volumes:
      - pgadmin:/var/lib/pgadmin
    ports:
      - "8090:80"
    networks:
      - webapi_network
    restart: unless-stopped
    depends_on:
      - postgres #It means if postgres up, so you can up,too

  ####################### REDIS #######################
  redis:
    image: redis:latest
    container_name: redis_container
    command: [ "redis-server", "/etc/redis/redis.conf" ] #the route of redis's configuration
    volumes:
      - ./redis/redis.conf:/etc/redis/redis.conf #copy redis's configuration to new route which defined
      - redis:/etc/redis #send this folder to my volume which defined before
    ports:
      - "6379:6379"
    networks:
      - webapi_network

  ##################### Monitoring #################
  prometheus:
    image: prom/prometheus:latest
    volumes:
      - ./prometheus/:/etc/prometheus/
      - prometheus_data:/prometheus
    command:
      - "--config.file=/etc/prometheus/prometheus.yml"
      - "--storage.tsdb.path=/prometheus"
      - "--web.console.libraries=/usr/share/prometheus/console_libraries"
      - "--web.console.templates=/usr/share/prometheus/consoles"
    ports:
      - 9090:9090
    links:
      - alertmanager:alertmanager
    networks:
      - webapi_network
    restart: always

  node-exporter:
    image: prom/node-exporter
    volumes:
      - /proc:/host/proc:ro
      - /sys:/host/sys:ro
      - /:/rootfs:ro
    command:
      - "--path.procfs=/host/proc"
      - "--path.sysfs=/host/sys"
      - --collector.filesystem.ignored-mount-points
      - "^/(sys|proc|dev|host|etc|rootfs/var/lib/docker/containers|rootfs/var/lib/docker/overlay2|rootfs/run/docker/netns|rootfs/var/lib/docker/aufs)($$|/)"
    ports:
      - 9100:9100
    networks:
      - webapi_network
    restart: always
    deploy:
      mode: global

  alertmanager:
    image: prom/alertmanager
    ports:
      - 9093:9093
    volumes:
      - ./alertmanager/:/etc/alertmanager/
    networks:
      - webapi_network
    restart: always
    command:
      - "--config.file=/etc/alertmanager/config.yml"
      - "--storage.path=/alertmanager"

  grafana:
    image: grafana/grafana
    user: "472"
    depends_on:
      - prometheus
    ports:
      - 3000:3000
    volumes:
      - grafana_data:/var/lib/grafana
      - ./grafana/provisioning/:/etc/grafana/provisioning/
    env_file:
      - ./grafana/config.monitoring
    networks:
      - webapi_network
    restart: always

  ##################### MyApplication #################
  car-api1:
    build: ../src/
    image: car-api:latest
    container_name: web-api1 #like the name of prometheus
    environment:
      - PORT=9001 #a port that you want to up swagger
    ports:
      - 9001:5000 #5000 is the port of swagger in docker(config/config-docker.yml) and 9001 is the port for being exposed
    networks:
      - webapi_network
    volumes:
      - logs:/app/logs #move /app/logs to the volume which name is logs
    depends_on:
      - postgres
      - elasticsearch
    restart: unless-stopped
  car-api2:
    build: ../src/
    image: car-api:latest
    container_name: web-api2
    environment:
      - PORT=9002
    ports:
      - 9002:5000
    networks:
      - webapi_network
    volumes:
      - logs:/app/logs
    depends_on:
      - postgres
      - elasticsearch
    restart: unless-stopped
####################### VOLUME AND NETWORKS #######################
volumes:
  postgres:
  pgadmin:
  redis:
  logs:
  setup:
  elasticsearch:
  prometheus_data:
  grafana_data:

networks:
  webapi_network:
    driver: bridge